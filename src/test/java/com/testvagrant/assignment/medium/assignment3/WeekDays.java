package com.testvagrant.assignment.medium.assignment3;

import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class WeekDays {

  public static Map<String, Integer> getMapOfDaysOccurrenceInTheMonth(List<String> daysList) {

    LocalDate localDate = LocalDate.now();
    int numberOfDaysInTheMonth = localDate.lengthOfMonth();
    String firstDayOfTheMonth =
        localDate.with(TemporalAdjusters.firstDayOfMonth()).getDayOfWeek().toString();

    // Initialize all counts as 4
    int[] count = new int[7];
    for (int i = 0; i < 7; i++) {
      count[i] = 4;
    }

    // find index of the first day
    int pos = 0;
    for (int i = 0; i < 7; i++) {
      if (firstDayOfTheMonth.equalsIgnoreCase(daysList.get(i))) {
        pos = i;
        break;
      }
    }

    // number of days whose occurrence will be 5
    int inc = numberOfDaysInTheMonth - 28;

    // mark the occurrence to be 5 of n-28 days
    for (int i = pos; i < pos + inc; i++) {
      if (i > 6) count[i % 7] = 5;
      else count[i] = 5;
    }

    // print the days
    Map<String, Integer> daysOccurrenceMap = new LinkedHashMap<>();

    for (int i = 0; i < 7; i++) {
      daysOccurrenceMap.put(daysList.get(i), count[i]);
    }

    return daysOccurrenceMap;
  }
}
