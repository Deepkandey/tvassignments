package com.testvagrant.assignment.medium.assignment3;

import com.testvagrant.assignment.utils.ExcelUtilities;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;
import java.time.DayOfWeek;
import java.util.*;

public class MonthlyExpenseTest {

  @Test(dataProvider = "itemList")
  public void getMonthlyExpense(String... items) throws IOException {

    List<String> daysList =
        List.of("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");

    Map<String, Map<String, Double>> itemAndDayWiseRateMap =
        ExcelUtilities.getItemAndDayWiseRateMap(daysList);

    Map<String, Integer> daysOccurrenceMap = WeekDays.getMapOfDaysOccurrenceInTheMonth(daysList);

    double newspaperExpense = 0;

    for (String item : items) {

      for (int i = 0; i < 7; i++) {

        if (!(daysList.get(i).equalsIgnoreCase(DayOfWeek.SATURDAY.toString())
            || daysList.get(i).equalsIgnoreCase(DayOfWeek.SUNDAY.toString()))) {

          newspaperExpense =
              newspaperExpense
                  + (itemAndDayWiseRateMap.get(item).get(daysList.get(i))
                      * daysOccurrenceMap.get(daysList.get(i)));
        }
      }
    }

    System.out.println(Arrays.asList(items) + " weekdays expense : " + newspaperExpense);
  }

  @DataProvider(name = "itemList")
  public Object[][] itemListProvider() {
    return new Object[][] {{"TOI", "Hindu", "HT"}, {"ET", "BM"}};
  }
}
