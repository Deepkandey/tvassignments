package com.testvagrant.assignment.utils;

import org.apache.poi.ss.usermodel.*;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ExcelUtilities {

  public static Map<String, Map<String, Double>> getItemAndDayWiseRateMap(List<String> daysList)
      throws IOException {

    Map<String, Map<String, Double>> itemAndDayWiseRateMap = new LinkedHashMap<>();

    FileInputStream fis = new FileInputStream("src/test/resources/EssentialItems.xlsx");
    Workbook workBook = WorkbookFactory.create(fis);
    fis.close();

    Sheet sheet = workBook.getSheet("Newspaper");

    for (int i = 1; i < sheet.getPhysicalNumberOfRows(); i++) {

      Row row = sheet.getRow(i);
      int cellNumbers = row.getPhysicalNumberOfCells();

      Cell cell = row.getCell(0);
      String itemName = cell.getStringCellValue();

      Map<String, Double> DayWiseRateMap = new LinkedHashMap<>();

      for (int j = 1; j < cellNumbers; j++) {
        Cell dayCell = row.getCell(j);
        double numericCellValue = dayCell.getNumericCellValue();
        DayWiseRateMap.put(daysList.get(j - 1), numericCellValue);
      }
      itemAndDayWiseRateMap.put(itemName, DayWiseRateMap);
    }
    return itemAndDayWiseRateMap;
  }
}
